#!/bin/sh

xhost local:root

if [ ! -f "$HOME/.config/sway/initialized" ]; then
    touch -m "$HOME/.config/sway/initialized"
    if which firefox-esr 2>&1 >/dev/null; then
        xdg-settings set default-web-browser firefox-esr.desktop
    fi
    swaycfg color-scheme dark
    swaymsg reload
fi
if [ ! -f "${XDG_STATE_HOME:-$HOME/.local/state}/weasley/dotstow" ]; then
    kitty -e weasley startup
fi
